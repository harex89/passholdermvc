﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PassHolderMVC.Migrations
{
    public partial class AddModyficationDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Modification",
                table: "PassDatas",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Modification",
                table: "PassDatas");
        }
    }
}
