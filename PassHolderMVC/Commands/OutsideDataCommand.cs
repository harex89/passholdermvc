﻿using MediatR;
using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.Commands
{
    public class OutsideDataCommand : INotification
    {
        public OutsideUserModel User { get; set; }
        public string Ip { get; set; }
        public int Port { get; set; }
    }

}
