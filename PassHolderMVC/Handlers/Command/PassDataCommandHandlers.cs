﻿using AutoMapper;
using MediatR;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using PassHolderMVC.Repositories;
using PassHolderMVC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PassHolderMVC.Handlers.Command
{
    public class PassDataCommandHandlers : 
        IRequestHandler<AddPassDataCommand, PassDataModel>,
        IRequestHandler<UpdatePassDataCommand, PassDataModel>,
        INotificationHandler<DeletePassDataCommand>,
        IRequestHandler<UpdateUserPassDataCommand, bool>,
        IRequestHandler<AddUserPassDataCommand, bool>
    {
        private readonly IPassDataRepository _passDataRepository;
        private readonly IMapper _mapper;
        private readonly ICryptoService _cryptoService;

        public PassDataCommandHandlers(IPassDataRepository passDataRepository, 
            IMapper mapper, 
            ICryptoService cryptoService)
        {
            _passDataRepository = passDataRepository;
            _mapper = mapper;
            _cryptoService = cryptoService;
        }

        public async Task<PassDataModel> Handle(AddPassDataCommand request, CancellationToken cancellationToken)
        {
            request.Password = _cryptoService.Encrypt(request.Password);
            request.Login = _cryptoService.Encrypt(request.Login);

            var entity = await _passDataRepository.SaveAsync(_mapper.Map<PassData>(request));

            var passData = _mapper.Map<PassDataModel>(entity);

            if (request.GroupId > 0)
            {
                var groupToPass = await _passDataRepository.AddPassDataToGroup(entity, request.GroupId);
                if (groupToPass != null)
                {
                    passData.GroupId = groupToPass.GroupId;
                }  
            }

            return passData;
        }

        public async Task<PassDataModel> Handle(UpdatePassDataCommand request, CancellationToken cancellationToken)
        {
            request.Password = _cryptoService.Encrypt(request.Password);
            request.Login = _cryptoService.Encrypt(request.Login);

            var entity = await _passDataRepository.SaveAsync(_mapper.Map<PassData>(request));

            if(request != null)
            {
                await _passDataRepository.AddPassDataToGroup(entity, request.GroupId);
            }

            return _mapper.Map<PassDataModel>(entity);
        }

        public async Task Handle(DeletePassDataCommand notification, CancellationToken cancellationToken)
        {
            await _passDataRepository.RemoveAsync(notification.Id);
        }

        public async Task<bool> Handle(UpdateUserPassDataCommand request, CancellationToken cancellationToken)
        {
            request.Password = _cryptoService.Encrypt(request.Password);
            request.Login = _cryptoService.Encrypt(request.Login);

            var entity = _mapper.Map<PassData>(request);

            await _passDataRepository.SaveAsync(entity);

            return true;
        }

        public async Task<bool> Handle(AddUserPassDataCommand request, CancellationToken cancellationToken)
        {
            request.Password = _cryptoService.Encrypt(request.Password);
            request.Login = _cryptoService.Encrypt(request.Login);

            var entity = _mapper.Map<PassData>(request);

            var response = await _passDataRepository.SaveAsync(entity);

            return true;
        }
    }
}
