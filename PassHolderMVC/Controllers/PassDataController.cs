﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassHolderMVC.Queries;
using PassHolderMVC.ViewModel;
using Microsoft.AspNetCore.Antiforgery;
using PassHolderMVC.PasswordGenerator;

namespace PassHolderMVC.Controllers
{
    public class PassDataController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private RoleManager<AppRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;
        private int _userId;
        private readonly IRandomPasswordGenerator _passGenerator;

        public PassDataController(UserManager<AppUser> userManager,
           SignInManager<AppUser> singInManager,
           RoleManager<AppRole> roleManager,
           IMapper mapper,
           IMediator mediator,
           IRandomPasswordGenerator passGenerator)
        {
            _userManager = userManager;
            _singInManager = singInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _mediator = mediator;
            _passGenerator = passGenerator;
        }


        public async Task<IActionResult> PassDataLoad()
        {
            ViewBag.Title = "PassData";

            _userId = int.Parse(_userManager.GetUserId(User));

            var passData = await _mediator.Send(new AllPassDataQuery() {UserId = _userId });

            var passDataVM = new PassDataViewModel() { PassDatas = passData };
            passDataVM.SelectedPassData = null;

            return View("PassData", passDataVM);
        }

        [HttpPost]
        public async Task<IActionResult> New()
        {

            ViewBag.Title = "PassData";

            _userId = int.Parse(_userManager.GetUserId(User));

            var passData = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });

            var passDataVM = new PassDataViewModel() { PassDatas = passData };
            passDataVM.SelectedPassData = new PassDataModel();
            passDataVM.DisplayMode = "WriteOnly";

            return View("PassData", passDataVM);
        }

        [HttpPost]
        public async Task<IActionResult> Insert(PassDataModel newPassData)
        {
            _userId = int.Parse(_userManager.GetUserId(User));

            if (await _mediator.Send(new ExistPassQuery(newPassData.Password)))
            {
                ViewBag.ErrorMessage = "This password exists in the database, to save the data enter another one.";

                ViewBag.Title = "PassData";

                var passDataserror = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });

                var passDataVM = new PassDataViewModel() { PassDatas = passDataserror };
                passDataVM.SelectedPassData = newPassData;
                passDataVM.DisplayMode = "WriteOnly";

                return View("PassData", passDataVM);
            }

            var passData = await _mediator.Send(new AddPassDataCommand()
            {
                Login = newPassData.Login,
                Name = newPassData.Name,
                Password = newPassData.Password,
                Description = newPassData.Description,
                GroupId = newPassData.GroupId
            });

            var passDatas = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });

            var model = new PassDataViewModel() { PassDatas = passDatas };

            model.SelectedPassData = passData;
            model.DisplayMode = "";

            return View("PassData", model);
        }

        [HttpPost]
        public async Task<IActionResult> Select(string id)
        {
            PassDataViewModel model = new PassDataViewModel();

            _userId = int.Parse(_userManager.GetUserId(User));

            model.PassDatas = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });
            model.SelectedPassData = await _mediator.Send(new PassDataQuery() { Id = int.Parse(id) });
            model.DisplayMode = "ReadOnly";

            return View("PassData", model);
        }

        [HttpPost]
        public async Task<IActionResult> SelectDelete(string id)
        {
            PassDataViewModel model = new PassDataViewModel();

            _userId = int.Parse(_userManager.GetUserId(User));

            model.PassDatas = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });
            model.SelectedPassData = await _mediator.Send(new PassDataQuery() { Id = int.Parse(id) });
            model.DisplayMode = "DeleteOnly";

            return View("PassData", model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id)
        {
            PassDataViewModel model = new PassDataViewModel();

            _userId = int.Parse(_userManager.GetUserId(User));

            model.PassDatas = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });
            model.SelectedPassData = await _mediator.Send(new PassDataQuery() { Id = int.Parse(id) });
            model.DisplayMode = "ReadWrite";

            return View("PassData", model);

        }

        [HttpPost]
        public async Task<IActionResult> Update(PassDataModel obj)
        {

            _userId = int.Parse(_userManager.GetUserId(User));

            if (await _mediator.Send(new ExistPassQuery(obj.Password)))
            {
                ViewBag.ErrorMessage = "This password exists in the database, to save the data enter another one.";

                ViewBag.Title = "PassData";

                var passDataserror = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });

                var passDataVM = new PassDataViewModel() { PassDatas = passDataserror };
                passDataVM.SelectedPassData = obj;
                passDataVM.DisplayMode = "ReadWrite";

                return View("PassData", passDataVM);
            }

            var passData = await _mediator.Send(new UpdatePassDataCommand()
            {
                Id = obj.Id,
                Login = obj.Login,
                Name = obj.Name,
                Password = obj.Password,
                Description = obj.Description,
                GroupId = obj.GroupId
            });

            PassDataViewModel model = new PassDataViewModel();

            model.PassDatas = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });

            model.SelectedPassData = passData;
            model.DisplayMode = "";

            return View("PassData", model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(PassDataModel obj)
        {
            await _mediator.Publish(new DeletePassDataCommand() { Id = obj.Id });

            PassDataViewModel model = new PassDataViewModel();

            model.PassDatas = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });

            model.SelectedPassData = null;
            model.DisplayMode = "";

            return View("PassData", model);
        }

        [HttpPost]
        public async Task<IActionResult> Cancel(PassDataModel obj)
        {
            PassDataViewModel model = new PassDataViewModel();

            _userId = int.Parse(_userManager.GetUserId(User));

            model.PassDatas = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });

            PassDataModel selectedPassData = new PassDataModel();

            if (obj.Id != 0)
            {
                selectedPassData = await _mediator.Send(new PassDataQuery() { Id = obj.Id });
            }

            model.SelectedPassData = selectedPassData;
            model.DisplayMode = "";

            return View("PassData", model);
        }

        public async Task<IActionResult> GetPasswordEdit(PassDataModel passData)
        {
            ViewBag.Title = "PassData";

            _userId = int.Parse(_userManager.GetUserId(User));

            var passDatas = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });

            var passDataVM = new PassDataViewModel() { PassDatas = passDatas };
            passDataVM.SelectedPassData = passData;
            passDataVM.SelectedPassData.Password = _passGenerator.GeneratePassword(true, true, true, true, 10);
            passDataVM.DisplayMode = "ReadWrite";

            return View("PassData", passDataVM);
        }

        public async Task<IActionResult> GetPasswordNew(PassDataModel passData)
        {
            ViewBag.Title = "PassData";

            _userId = int.Parse(_userManager.GetUserId(User));

            var passDatas = await _mediator.Send(new AllPassDataQuery() { UserId = _userId });

            var passDataVM = new PassDataViewModel() { PassDatas = passDatas };
            passDataVM.SelectedPassData = passData;
            passDataVM.SelectedPassData.Password = _passGenerator.GeneratePassword(true, true, true, true, 10);
            passDataVM.DisplayMode = "WriteOnly";

            return View("PassData", passDataVM);
        }


    }
}
