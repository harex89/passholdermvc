﻿using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.ViewModel
{
    public class UsersViewModel
    {
        public IEnumerable<UserModel> Users { get; set; }
        public IEnumerable<RoleModel> Roles { get; set; }
    }
}
