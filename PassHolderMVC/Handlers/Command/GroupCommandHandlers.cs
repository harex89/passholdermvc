﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using PassHolderMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PassHolderMVC.Handlers.Command
{
    public class GroupCommandHandlers : 
        IRequestHandler<AddGroupCommand, GroupModel>,
        IRequestHandler<AddUserToGroupCommand, bool>,
        IRequestHandler<DeleteUserToGroupCommand, bool>,
        IRequestHandler<DeleteGroupCommand, bool>,
        IRequestHandler<EditGroupCommand, bool>
    {
        private readonly IGroupsRepository _groupsRepository;
        private readonly IMapper _mapper;

        public GroupCommandHandlers(IGroupsRepository groupsRepository, IMapper mapper)
        {
            _groupsRepository = groupsRepository;
            _mapper = mapper;
        }

        public async Task<GroupModel> Handle(AddGroupCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Group>(request);

            var group = await _groupsRepository.SaveAsync(entity);

            return _mapper.Map<GroupModel>(group);
        }

        public async Task<bool> Handle(AddUserToGroupCommand request, CancellationToken cancellationToken)
        {
            var response = _groupsRepository.AddUserToGroup(request.UserId, request.GroupId);

            return response;
        }

        public async Task<bool> Handle(DeleteUserToGroupCommand request, CancellationToken cancellationToken)
        {
            var response = _groupsRepository.RemoveUserToGroup(request.UserId, request.GroupId);

            return response;
        }

        public async Task<bool> Handle(DeleteGroupCommand request, CancellationToken cancellationToken)
        {
            var response = _groupsRepository.Remove(request.GroupId);

            return response;
        }

        public async Task<bool> Handle(EditGroupCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Group>(request.Group);

            var response = await _groupsRepository.UpdateGroupAsync(entity);

            return response;
        }
    }
}
