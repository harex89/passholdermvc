﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.Models
{
    public class LogInUserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
