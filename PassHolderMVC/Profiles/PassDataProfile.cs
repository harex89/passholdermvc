﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Routing.Constraints;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;


namespace PassHolderMVC.Profiles
{
    public class PassDataProfile : Profile
    {
        public PassDataProfile()
        {
            AllowNullCollections  = true;

            CreateMap<PassDataModel, PassData>()
                .ReverseMap();

            CreateMap<AddPassDataCommand, PassData>()
                .ReverseMap();

            CreateMap<UpdatePassDataCommand, PassData>()
                .ReverseMap();

            CreateMap<UpdateUserPassDataCommand, PassData>()
                .ForMember(dest => dest.AppUserId, opt => opt.MapFrom(src => src.UserId))
                .ReverseMap(); 

            CreateMap<AddUserPassDataCommand, PassData>()
                .ForMember(dest => dest.AppUserId, opt => opt.MapFrom(src => src.UserId))
                .ReverseMap();
        }
    }
}
