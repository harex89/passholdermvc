﻿using MediatR;
using PassHolderMVC.Models;
using PassHolderMVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.Queries
{
    public class AllGroupQuery : IRequest<List<GroupModel>>
    {
    }

    public class ByIdGroupEditViewModelQuery : IRequest<SelectEditGroupViewModel>
    {
        public int Id { get; set; }
    }

    public class ByIdGroupQuery : IRequest<GroupModel>
    {
        public int Id { get; set; }
    }
}
