﻿using PassHolderMVC.Entities;
using PassHolderMVC.Exceptions;
using PassHolderMVC.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PassHolderMVC.Repositories
{
    public interface IPassDataRepository
    {
        Task<List<PassData>> GetAllAsync();
        Task<List<PassData>> GetAllByUserGroupsAsync(int userId);
        Task<List<PassData>> GetByUserIdAsync(int userId);
        Task<PassData> GetAsync(int id);
        Task<PassData> SaveAsync(PassData passData);
        Task<GroupsToPass> AddPassDataToGroup(PassData passData, int groupId);
        Task RemoveAsync(int id);
        Task<List<PassData>> CheckPassExist();
    }
    public class PassDataRepository : IPassDataRepository
    {
        private readonly ApplicationDbContext _context;

        public PassDataRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<GroupsToPass> AddPassDataToGroup(PassData passData, int groupId)
        {
            GroupsToPass groupToPass = new GroupsToPass();

            if (passData != null)
            {

                groupToPass = await _context.GroupsToPasses.AsNoTracking().FirstOrDefaultAsync(gtp => gtp.PassDataId == passData.Id && gtp.GroupId == groupId);

                if (groupToPass == null)
                {
                    groupToPass = new GroupsToPass()
                    {
                        GroupId = groupId,
                        PassDataId = passData.Id
                    };

                    _context.Add(groupToPass);
                }
                else
                {
                    groupToPass.GroupId = groupId;
                    groupToPass.PassDataId = passData.Id;

                    _context.Update(groupToPass);
                }

                await _context.SaveChangesAsync();

            }

            return groupToPass;
        }

        public async Task<List<PassData>> CheckPassExist()
        {
            return  _context.PassDatas.AsNoTracking().ToList();
        }

        public async Task<List<PassData>> GetAllAsync()
        {
            return await _context.PassDatas.AsNoTracking()
                .Include(pd => pd.GroupsToPasses)
                .ToListAsync();
        }

        public async Task<List<PassData>> GetAllByUserGroupsAsync(int userId)
        {
            var gropsIds = _context.GroupsToUsers
                .Where(gtu => gtu.AppUserId == userId)
                .Select(gtu => gtu.GroupId)
                .ToList<int>();

            return await _context.PassDatas.Include(p => p.GroupsToPasses)
                .Where(p => p.GroupsToPasses.Any(gtp => gropsIds.Any(gi => gi == gtp.GroupId)))
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<PassData> GetAsync(int id)
        {
            return await _context.PassDatas.AsNoTracking()
                .Include(pd => pd.GroupsToPasses)
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<PassData>> GetByUserIdAsync(int userId)
        {
            return await _context.PassDatas.AsNoTracking().Where(p => p.AppUserId == userId).ToListAsync();
        }

        public  Task RemoveAsync(int id)
        {
            var passDataToDelete =  _context.PassDatas.AsNoTracking().FirstOrDefault(r => r.Id == id);
            if (passDataToDelete == null)
            {
                return  Task.CompletedTask;
            }
            _context.PassDatas.Remove(passDataToDelete);
            return _context.SaveChangesAsync();

        }

        public async Task<PassData> SaveAsync(PassData passData)
        {
            passData.Modification = DateTime.Now;
            if (passData.Id == 0)
            {
                _context.Add(passData);
            }
            else
            {
                _context.Update(passData);
            }

            await _context.SaveChangesAsync();

            return passData;
        }
    }
}
