﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassHolderMVC.Queries;
using PassHolderMVC.ViewModel;

namespace PassHolderMVC.Controllers
{
    public class GroupController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private RoleManager<AppRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public GroupController(UserManager<AppUser> userManager, 
           SignInManager<AppUser> singInManager,
           RoleManager<AppRole> roleManager,
           IMapper mapper,
           IMediator mediator)
        {
            _userManager = userManager;
            _singInManager = singInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _mediator = mediator;
        }


        public async Task<IActionResult> GroupsLoad()
        {

            ViewBag.Title = "Groups";
            var groups = await _mediator.Send(new AllGroupQuery());


            return View("Groups", groups);
        }

        public async Task<IActionResult> SelectNew()
        {
            ViewBag.Title = "New Group";

            return View("GroupsNew", new GroupModel());
        }

        public async Task<IActionResult> Insert(GroupModel newGroup)
        {
            var grouo = await _mediator.Send(new AddGroupCommand() { Name = newGroup.Name});

            var groups = await _mediator.Send(new AllGroupQuery());

            return View("Groups", groups);
        }

        public async Task<IActionResult> SelectEdit(string id)
        {

            ViewBag.Title = "Groups";
            var group = await _mediator.Send(new ByIdGroupEditViewModelQuery() { Id = int.Parse(id) });

            return View("GroupsEdit", group);
        }

        public async Task<IActionResult> AddUser(SelectEditGroupViewModel viewModel)
        {
            var response = await _mediator.Send(new AddUserToGroupCommand() {UserId = viewModel.NewUserId, GroupId = viewModel.Group.Id });

            ViewBag.Title = "User Added";
            var newViewModel = await _mediator.Send(new ByIdGroupEditViewModelQuery() { Id = viewModel.Group.Id });

            return View("GroupsEdit", newViewModel);
        }

        public async Task<IActionResult> DeleteUser(string userId, string groupId)
        {
            var response = await _mediator
                .Send(new DeleteUserToGroupCommand() { UserId = int.Parse(userId), GroupId = int.Parse(groupId) });

            ViewBag.Title = "User Added";
            var newViewModel = await _mediator.Send(new ByIdGroupEditViewModelQuery() { Id = int.Parse(groupId) });

            return View("GroupsEdit", newViewModel);
        }

        public async Task<IActionResult> EditGroup(SelectEditGroupViewModel groupViewModel)
        {
            ViewBag.Title = "Groups";

            var response = await _mediator.Send(new EditGroupCommand() { Group = groupViewModel.Group });

            return RedirectToAction("GroupsLoad", "Group");
        }

        public async Task<IActionResult> SelectDelete(string id)
        {
            ViewBag.Title = "Groups";

            var group = await _mediator.Send(new ByIdGroupQuery() { Id = int.Parse(id) });

            var newViewModel = new SelectDeleteGroupViewModel() { Group = group };

            return View("GroupsDelete", newViewModel);
        }

        public async Task<IActionResult> DeleteGroup(string groupId)
        {
            ViewBag.Title = "Groups";

            var response = await _mediator.Send(new DeleteGroupCommand() { GroupId = int.Parse(groupId)});

            return RedirectToAction("GroupsLoad", "Group");
        }

    }
}
