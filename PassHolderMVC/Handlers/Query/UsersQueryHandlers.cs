﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using PassHolderMVC.Queries;
using PassHolderMVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PassHolderMVC.Handlers.Query
{
    public class UsersQueryHandlers : IRequestHandler<AllUsersManagerQuery, UsersViewModel>, 
        IRequestHandler<UserQuery, UserModel>
    {
        private readonly IMapper _mapper;
        private UserManager<AppUser> _userManager;
        private RoleManager<AppRole> _roleManager;

        public UsersQueryHandlers(IMapper mapper, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<UsersViewModel> Handle(AllUsersManagerQuery request, CancellationToken cancellationToken)
        {
            var usersEntity = await _userManager.Users.ToListAsync();

            var users = _mapper.Map<List<UserModel>>(usersEntity);

            var rolesEntity = await _roleManager.Roles.ToListAsync();

            var roles = _mapper.Map<List<RoleModel>>(rolesEntity);

            users.ForEach(u =>
            {
                u.role.Name = roles.FirstOrDefault(r => r.Id == u.role.Id).Name;
            });

            return new UsersViewModel() { Users = users, Roles = roles };
        }

        public async Task<UserModel> Handle(UserQuery request, CancellationToken cancellationToken)
        {
            var entity = _userManager.Users.First(u => u.Id == request.UserId);

            var user = _mapper.Map<UserModel>(entity);

            return user;
        }
    }
}
