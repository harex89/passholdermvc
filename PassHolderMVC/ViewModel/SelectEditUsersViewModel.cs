﻿using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.ViewModel
{
    public class SelectEditGroupViewModel
    {
        public GroupModel Group { get; set; }
        public List<SimpleUserModel> AvaibleUsers { get; set; }
        public int NewUserId { get; set; }
    }
}
