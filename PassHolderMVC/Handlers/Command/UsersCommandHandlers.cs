﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PassHolderMVC.Handlers.Command
{
    public class UsersCommandHandlers : 
        INotificationHandler<DeleteUserCommand>,
        IRequestHandler<EditUserCommand, UserModel>
    {
        private readonly IMapper _mapper;
        private UserManager<AppUser> _userManager;
        private RoleManager<AppRole> _roleManager;

        public UsersCommandHandlers(IMapper mapper, 
            UserManager<AppUser> userManager, 
            RoleManager<AppRole> roleManager)
        {
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task Handle(DeleteUserCommand notification, CancellationToken cancellationToken)
        {
            var appUser = _userManager.Users.First(u => u.Id == notification.UserId);
            await _userManager.DeleteAsync(appUser);
        }

        public async Task<UserModel> Handle(EditUserCommand request, CancellationToken cancellationToken)
        {
            var entity = _userManager.Users.First(u => u.Id == request.UserModel.Id);

            entity.FirstName = request.UserModel.FirstName;
            entity.LastName = request.UserModel.LastName;
            entity.RoleId = request.UserModel.role.Id;

            var newUser = await _userManager.UpdateAsync(entity);

            return _mapper.Map<UserModel>(entity);
        }
    }
}
