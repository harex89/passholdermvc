﻿using ExternalAuthenticationTester.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExternalAuthenticationTester.Controllers
{
    [Route("api/externallogin")]
    [ApiController]
    public class ExternalLoginController : ControllerBase
    {

        [HttpPost("Authentication")]
        public IActionResult Authentication([FromBody] ExternalUserModel user)
        {
            return RedirectToAction("ShowUserData", "Home", user);
        }
    }
}
