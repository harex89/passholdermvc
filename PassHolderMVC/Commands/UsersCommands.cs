﻿using MediatR;
using PassHolderMVC.Models;
using PassHolderMVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace PassHolderMVC.Commands
{
    public class DeleteUserCommand : INotification
    {
        public int UserId { get; set; }
    }

    public class EditUserCommand : IRequest<UserModel>
    {
        public UserModel UserModel { get; set; }

        public EditUserCommand(SelectEditUsersViewModel userViewModel)
        {
            UserModel = userViewModel.User;
        }
    }
}
