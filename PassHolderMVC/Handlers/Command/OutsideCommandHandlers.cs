﻿using MediatR;
using PassHolderMVC.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace PassHolderMVC.Handlers.Command
{
    public class OutsideCommandHandlers : INotificationHandler<OutsideDataCommand>
    {
        private readonly HttpClient _client;

        public OutsideCommandHandlers()
        {
            _client = new HttpClient();
        }

        public async Task Handle(OutsideDataCommand notification, CancellationToken cancellationToken)
        {
            if (notification.Ip.Equals("::1"))
            {
                notification.Ip = "localhost";
            }

            var url = "https://" + notification.Ip + ":" + notification.Port + "/api/externallogin/authentication";

            var content = new StringContent(JsonSerializer.Serialize(notification.User), Encoding.UTF8, "application/json");

            await _client.PostAsync(url, content);
        }
    } 
}
