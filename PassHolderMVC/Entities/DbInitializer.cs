﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace PassHolderMVC.Entities
{
    public class DbInitializer
    {
        public static void Seed(ApplicationDbContext context)
        {
            if (!context.Roles.Any())
            {
                context.AddRange
                    (
                        new AppRole() { Name = "SuperAdmin", NormalizedName = "superadmin"},
                        new AppRole() { Name = "Admin", NormalizedName = "admin" },
                        new AppRole() { Name = "User", NormalizedName = "user" }
                    );
                context.SaveChanges();
            }
        }
    }
}
