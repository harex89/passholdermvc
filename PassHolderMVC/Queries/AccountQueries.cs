﻿using MediatR;
using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.Queries
{
    public class AllUsersQuery : IRequest<List<UserModel>>
    {
    }

    public class UserQuery : IRequest<UserModel>
    {
        public int UserId { get; set; }
    }
}
