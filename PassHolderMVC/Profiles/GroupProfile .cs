﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Routing.Constraints;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;


namespace PassHolderMVC.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            AllowNullCollections  = true;

            CreateMap<GroupModel, Group>()
                .ReverseMap();

            CreateMap<AddGroupCommand, Group>()
                .ReverseMap();

            CreateMap<SimpleUserModel, GroupToUser>()
                .ForMember(dest => dest.IsModerator, opt => opt.MapFrom(src => src.IsAdmin))
                .ForMember(dest => dest.AppUserId, opt => opt.MapFrom(src => src.Id))
                .ForPath(dest => dest.AppUser.UserName, opt => opt.MapFrom(src => src.UserName))                
                .ReverseMap();
        }
    }
}
