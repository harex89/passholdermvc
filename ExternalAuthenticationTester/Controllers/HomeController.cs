﻿using ExternalAuthenticationTester.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ExternalAuthenticationTester.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login()
        {
            var _client = new HttpClient();

            string url = "https://localhost:5001/api/outside/login/" + HttpContext.Connection.LocalPort.ToString(); ;

            return Redirect(url);
        }

        public IActionResult ShowUserData(ExternalUserModel userModel)
        {
            return Redirect("ShowUserData", userModel);
        }

    }
}
