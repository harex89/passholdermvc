﻿using MediatR;
using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.Queries
{
    public class AllPassDataQuery : IRequest<List<PassDataModel>>
    {
        public int UserId { get; set; }
    }

    public class PassDataQuery : IRequest<PassDataModel>
    {
        public int Id { get; set; }
    }

    public class PassDataByUserQuery : IRequest<List<PassDataModel>>
    {
        public int UserId { get; set; }
    }

    public class ExistPassQuery : IRequest<bool>
    {
        public ExistPassQuery(string password)
        {
            Password = password;
        }

        public string Password { get; set; }
    }
}
