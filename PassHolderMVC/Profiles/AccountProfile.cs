﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;


namespace PassHolderMVC.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<AppUser, RegisterUserCommand>()
                .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.RoleId))
                .ReverseMap();
            CreateMap<RegisterUserModel, RegisterUserCommand>()
                .ReverseMap();
            CreateMap<LogInUserCommand, LogInUserModel>()
                .ReverseMap();
            CreateMap<UserModel, AppUser>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                    .ForPath(dest => dest.RoleId, opt => opt.MapFrom(src => src.role.Id))
                .ReverseMap();
            CreateMap<AppRole, RoleModel>()
                .ReverseMap();
        }
    }
}
