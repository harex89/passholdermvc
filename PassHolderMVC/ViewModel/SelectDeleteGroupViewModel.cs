﻿using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.ViewModel
{
    public class SelectDeleteGroupViewModel
    {
        public GroupModel Group { get; set; }
    }
}
