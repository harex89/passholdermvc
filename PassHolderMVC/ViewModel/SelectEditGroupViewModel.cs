﻿using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.ViewModel
{
    public class SelectEditUsersViewModel
    {
        public UserModel User { get; set; }

        public List<GroupModel> UserGroups{ get; set; }

        public List<GroupModel> Groups { get; set; }

        public List<RoleModel> Roles { get; set; }
    }
}
