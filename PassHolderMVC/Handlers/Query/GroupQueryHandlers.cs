﻿using PassHolderMVC.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PassHolderMVC.Models;
using Microsoft.AspNetCore.Identity;
using PassHolderMVC.Entities;
using System.Threading;
using AutoMapper;
using PassHolderMVC.Queries;
using PassHolderMVC.Repositories;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore;
using PassHolderMVC.ViewModel;

namespace PassHolderMVC.Handlers.Query
{
    public class GroupQueryHandlers : 
        IRequestHandler<AllGroupQuery, List<GroupModel>>,
        IRequestHandler<ByIdGroupEditViewModelQuery, SelectEditGroupViewModel>,
        IRequestHandler<ByIdGroupQuery, GroupModel>
    {

        private readonly IMapper _mapper;
        private readonly IGroupsRepository _groupsRepository;
        private UserManager<AppUser> _userManager;

        public GroupQueryHandlers(IMapper mapper, 
            IGroupsRepository groupsRepository, 
            UserManager<AppUser> userManager)
        {
            _mapper = mapper;
            _groupsRepository = groupsRepository;
            _userManager = userManager;
        }

        public async Task<List<GroupModel>> Handle(AllGroupQuery request, CancellationToken cancellationToken)
        {

            var entities = await _groupsRepository.GetAllAsync();
            var groups =  _mapper.Map<List<GroupModel>>(entities);

            foreach (var group in groups)
            {
                group.users = _mapper.Map<List<SimpleUserModel>>(entities.First(e => e.Id == group.Id).GroupsToUsers.ToList());
            }

            return groups;
        }

        public async Task<SelectEditGroupViewModel> Handle(ByIdGroupEditViewModelQuery request, CancellationToken cancellationToken)
        {
            var entity = await _groupsRepository.GetAsync(request.Id);

            var group = _mapper.Map<GroupModel>(entity);

            var usersEntities = await _userManager.Users.ToListAsync();

            if (entity.GroupsToUsers != null)
            {
                group.users = new List<SimpleUserModel>();

                foreach (var user in entity.GroupsToUsers)
                {
                    var userEntity = usersEntities.FirstOrDefault(u => u.Id == user.AppUserId);

                    group.users.Add(new SimpleUserModel()
                    {
                        Id = userEntity.Id,
                        UserName = userEntity.UserName,
                        IsAdmin = userEntity.RoleId > 0 ? true : false
                    });

                    usersEntities.Remove(userEntity);
                }
            }

            List<SimpleUserModel> avaibleUsers = new List<SimpleUserModel>();

            usersEntities.ForEach(ue =>
            {
                avaibleUsers.Add(new SimpleUserModel()
                {
                    Id = ue.Id,
                    UserName = ue.UserName,
                    IsAdmin = ue.RoleId > 0 ? true : false
                });
                
            });

            SelectEditGroupViewModel vievModel = new SelectEditGroupViewModel()
            {
                Group = _mapper.Map<GroupModel>(group),
                AvaibleUsers = avaibleUsers,
                NewUserId = 0
            };

            return vievModel;
        }

        public async Task<GroupModel> Handle(ByIdGroupQuery request, CancellationToken cancellationToken)
        {
            var entity = await _groupsRepository.GetAsync(request.Id);

            return _mapper.Map<GroupModel>(entity);
        }
    }
}
