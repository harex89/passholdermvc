﻿using MediatR;
using PassHolderMVC.Models;
using PassHolderMVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.Queries
{
    public class AllUsersManagerQuery : IRequest<UsersViewModel>
    {
    }
}
