﻿using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassHolderMVC.Queries;
using PassHolderMVC.ViewModel;
using System.Collections.Generic;

namespace PassHolderMVC.Controllers
{
    public class UsersController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private RoleManager<AppRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public UsersController(UserManager<AppUser> userManager,
            SignInManager<AppUser> singInManager, 
            RoleManager<AppRole> roleManager, 
            IMapper mapper, IMediator mediator)
        {
            _userManager = userManager;
            _singInManager = singInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<IActionResult> UsersLoad()
        {
            ViewBag.Title = "Users";
            var users = await _mediator.Send(new AllUsersManagerQuery());

            return View("Users", users);
        }

        public async Task<IActionResult> SelectDelete(string id)
        {
            ViewBag.Title = "Delete Users";
            var user = await _mediator.Send(new UserQuery() {UserId = int.Parse(id) });

            return View("UsersDelete", new SelectDeleteUserViewModel() {User = user });
        }

        public async Task<IActionResult> DeleteUser(string userId)
        {
            await _mediator.Publish(new DeleteUserCommand() { UserId = int.Parse(userId) });

            return RedirectToAction("UsersLoad", "Users");
        }

        public async Task<IActionResult> SelectEdit(string id)
        {
            ViewBag.Title = "Edit Users";
            var user = await _mediator.Send(new UserQuery() { UserId = int.Parse(id) });

            var roles = _roleManager.Roles.ToList();

            user.role.Name = roles.First(r => r.Id == user.role.Id).Name;

            roles.Remove(roles.First(r => r.Id == user.role.Id));

            return View("UsersEdit", new SelectEditUsersViewModel() { User = user, Roles = _mapper.Map<List<RoleModel>>(roles) });
        }

        public async Task<IActionResult> EditUser(SelectEditUsersViewModel userViewModel)
        {
            var user = await _mediator.Send(new EditUserCommand(userViewModel));

            return RedirectToAction("UsersLoad", "Users");
        }
    }
}
