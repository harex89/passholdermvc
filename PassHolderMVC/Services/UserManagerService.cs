﻿using Microsoft.AspNetCore.Identity;
using PassHolderMVC.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace PassHolderMVC.Services
{
    public interface IUserManagerService
    {
        bool IsInRolebyId(ClaimsPrincipal principal, int roleIdToCheck);

    }

    public class UserManagerService : IUserManagerService
    {
        private readonly UserManager<AppUser> _userManager;

        public UserManagerService(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public bool IsInRolebyId(ClaimsPrincipal principal, int roleIdToCheck)
        {
            int roleId = 0;

            if (principal != null)
            {
                var userId = int.Parse(_userManager.GetUserId(principal));
                roleId = _userManager.Users.First(u => u.Id == userId).RoleId;
            }

            return roleId == roleIdToCheck;
        }
    }
}
