﻿using MediatR;
using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.Commands
{
    public class AddGroupCommand : IRequest<GroupModel>
    {
        public string Name { get; set; }
    }

    public class AddUserToGroupCommand : IRequest<bool>
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }
    }

    public class DeleteUserToGroupCommand : IRequest<bool>
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }
    }

    public class EditGroupCommand : IRequest<bool>
    {
        public GroupModel Group { get; set; }
    }

    public class DeleteGroupCommand : IRequest<bool>
    {
        public int GroupId { get; set; }
    }
}
