﻿using PassHolderMVC.Entities;
using PassHolderMVC.Exceptions;
using PassHolderMVC.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Group = PassHolderMVC.Entities.Group;
using System.Runtime.InteropServices.ComTypes;

namespace PassHolderMVC.Repositories
{
    public interface IGroupsRepository
    {
        Task<List<Group>> GetAllAsync();
        Task<List<Group>> GetAllByUserIdAsync(string userId);
        Task<Group> SaveAsync(Group group);
        bool Remove(int groupId);
        Task<Group> GetAsync(int id);
        bool AddUserToGroup(int userId, int groupId);
        bool RemoveUserToGroup(int userId, int groupId);
        Task<bool> UpdateGroupAsync(Group group);
    }
    public class GroupsRepository : IGroupsRepository
    {
        private readonly ApplicationDbContext _context;

        public GroupsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool AddUserToGroup(int userId, int groupId)
        {
            _context.GroupsToUsers.Add(new GroupToUser() { AppUserId = userId, GroupId = groupId });

            _context.SaveChanges();

            return true;
        }

        public async Task<List<Group>> GetAllAsync()
        {  
            return await _context.Groups
                .Include(g => g.GroupsToUsers)
                .ThenInclude(gtu => gtu.AppUser)
                .Include(g => g.GroupsToPasses)
                .ThenInclude(gtp => gtp.PassData)
                .ToListAsync();
        }

        public async Task<List<Group>> GetAllByUserIdAsync(string userId)
        {
            return await _context.Groups
                .Include(g => g.GroupsToUsers)
                .ThenInclude(gtu => gtu.AppUser)
                .Include(g => g.GroupsToPasses)
                .ThenInclude(gtp => gtp.PassData)
                .Where(g => g.GroupsToUsers.Any(gtu => gtu.AppUserId == int.Parse(userId)))
                .ToListAsync();
        }

        public async Task<Group> GetAsync(int id)
        {
            return await _context.Groups.Include(g => g.GroupsToUsers).FirstOrDefaultAsync(g => g.Id == id);
        }

        public bool Remove(int groupId)
        {
            var group = _context.Groups.FirstOrDefault(g => g.Id == groupId);

            if(groupId != null)
            {
                _context.Remove(group);

                _context.SaveChanges();
            }

            return true;
        }

        public bool RemoveUserToGroup(int userId, int groupId)
        {
            var userToGroup = _context
                .GroupsToUsers
                .FirstOrDefault(utg => utg.AppUserId == userId && utg.GroupId == groupId);

            if (userToGroup != null)
            {
                _context.Remove(userToGroup);

                _context.SaveChanges();
            }

            return true;
        }

        public async Task<Group> SaveAsync(Group group)
        {
            if (group.GroupsToPasses == null)
                group.GroupsToPasses = new List<GroupsToPass>();

            if (group.GroupsToUsers == null)
                group.GroupsToUsers = new List<GroupToUser>();

            if(group.Id != null && group.Id != 0)
            {
                _context.Groups.Update(group);
            }
            else
            {
                _context.Groups.Add(group);
            }

            await _context.SaveChangesAsync();

            return group;
        }

        public async Task<bool> UpdateGroupAsync(Group group)
        {
            var entity = await _context.Groups.FirstAsync(g => g.Id == group.Id);

            entity.Name = group.Name;

            await _context.SaveChangesAsync();

            return true;
        }
    }
}
