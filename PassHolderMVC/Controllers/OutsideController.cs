﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using PassHolderMVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.Controllers
{
    [Route("api")]
    [ApiController]
    public class OutsideController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private IMediator _mediatR;

        public OutsideController(UserManager<AppUser> userManager, SignInManager<AppUser> singInManager, IMediator mediatR)
        {
            _userManager = userManager;
            _singInManager = singInManager;
            _mediatR = mediatR;
        }

        [HttpGet("outside/login/{remotePort}")]
        public IActionResult Authentication([FromRoute] int remotePort)
        {
            var ip = HttpContext.Connection.RemoteIpAddress.ToString();

            return View("Login", new OutsideViewModel() {Ip = ip, Port = remotePort, UserModel = new LogInUserModel() });
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromForm] OutsideViewModel command)
        {
            //var massage = await _mediator.Send(_mapper.Map<LogInUserCommand>(command));
            var user = await _userManager.FindByNameAsync(command.UserModel.UserName);

            var massage = await _singInManager.PasswordSignInAsync(user, command.UserModel.Password, false, false);
            if (massage.Succeeded)
            {
                var userToSend = new OutsideUserModel() { 
                    Id = user.Id,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    UserName = user.UserName
                };

                _mediatR.Publish(new OutsideDataCommand() { Ip = command.Ip, Port = command.Port, User = userToSend });

                return Ok();
            }
            else
            {
                ViewBag.Result = massage.ToString();

                return View(new OutsideUserModel());
            }
        }
    }
}
