﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using PassHolderMVC.Queries;
using PassHolderMVC.Repositories;
using PassHolderMVC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Threading;
using System.Threading.Tasks;

namespace PassHolderMVC.Handlers.Query
{
    public class PassDataQueryHandlers : 
        IRequestHandler<AllPassDataQuery, List<PassDataModel>>, 
        IRequestHandler<PassDataQuery, PassDataModel>,
        IRequestHandler<PassDataByUserQuery, List<PassDataModel>>,
        IRequestHandler<ExistPassQuery, bool>
    {

        private readonly IPassDataRepository _passDataRepository;
        private readonly IMapper _mapper;
        private readonly ICryptoService _cryptoService;
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private RoleManager<AppRole> _roleManager;

        public PassDataQueryHandlers(IPassDataRepository passDataRepository, 
            IMapper mapper, 
            ICryptoService cryptoService, 
            UserManager<AppUser> userManager, 
            SignInManager<AppUser> singInManager, 
            RoleManager<AppRole> roleManager)
        {
            _passDataRepository = passDataRepository;
            _mapper = mapper;
            _cryptoService = cryptoService;
            _userManager = userManager;
            _singInManager = singInManager;
            _roleManager = roleManager;
        }

        public async Task<List<PassDataModel>> Handle(AllPassDataQuery request, CancellationToken cancellationToken)
        {
            

            var entities = await _passDataRepository.GetAllByUserGroupsAsync(request.UserId);

            var passDatas = _mapper.Map<List<PassDataModel>>(entities);

            foreach (var entity in entities)
            {
                if (entity.GroupsToPasses != null && entity.GroupsToPasses.Count() > 0)
                {
                    passDatas.First(pd => pd.Id == entity.Id).GroupId = entity.GroupsToPasses.First().GroupId;
                }
            }

            passDatas.ForEach(p =>
            {
                p.Login = _cryptoService.Decrypt(p.Login);
                p.Password = _cryptoService.Decrypt(p.Password);
            });

            return passDatas;
        }

        public async Task<PassDataModel> Handle(PassDataQuery request, CancellationToken cancellationToken)
        {
            var entity = await _passDataRepository.GetAsync(request.Id);

            var passData = _mapper.Map<PassDataModel>(entity);

            if (entity != null && entity.GroupsToPasses != null && entity.GroupsToPasses.Count() > 0)
            {
                passData.GroupId = entity.GroupsToPasses.FirstOrDefault().GroupId;
            }

            if(passData?.UserId != null)
            {
                passData.Login = _cryptoService.Decrypt(passData.Login);
                passData.Password = _cryptoService.Decrypt(passData.Password);
            }
            else
            {
                passData.Login = _cryptoService.Decrypt(passData.Login);
                passData.Password = _cryptoService.Decrypt(passData.Password);
            }



            return passData;
        }

        public async Task<List<PassDataModel>> Handle(PassDataByUserQuery request, CancellationToken cancellationToken)
        {
            var entity = await _passDataRepository.GetByUserIdAsync(request.UserId);

            var passData = _mapper.Map<List<PassDataModel>>(entity);

            passData.ForEach(p =>
            {
                p.Login = _cryptoService.Decrypt(p.Login);
                p.Password = _cryptoService.Decrypt(p.Password);
            });

            return passData;
        }

        public async Task<bool> Handle(ExistPassQuery request, CancellationToken cancellationToken)
        {

            var datas = await _passDataRepository.CheckPassExist();

            var response = datas.Any(d => _cryptoService.Decrypt(d.Password) == request.Password);

            return response;
        }
    }
}
