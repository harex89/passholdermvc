﻿using PassHolderMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolderMVC.ViewModel
{
    public class OutsideViewModel
    {
        public string Ip { get; set; }
        public int Port { get; set; }
        public LogInUserModel UserModel { get; set; }
    }
}
