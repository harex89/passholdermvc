﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassHolderMVC.Commands;
using PassHolderMVC.Entities;
using PassHolderMVC.Models;
using PassHolderMVC.PasswordGenerator;
using PassHolderMVC.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace PassHolderMVC.Controllers
{
    public class UserPassDataController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private RoleManager<AppRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;
        private readonly IRandomPasswordGenerator _passGenerator;

        public UserPassDataController(UserManager<AppUser> userManager, 
            SignInManager<AppUser> singInManager, 
            RoleManager<AppRole> roleManager, 
            IMapper mapper, IMediator mediator,
            IRandomPasswordGenerator passGenerator)
        {
            _userManager = userManager;
            _singInManager = singInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _mediator = mediator;
            _passGenerator = passGenerator;
        }

        public async Task<IActionResult> PassDataLoad()
        {
            var userId = _userManager.GetUserId(User);

            ViewBag.Title = "User PassData";
            var passData = await _mediator.Send(new PassDataByUserQuery() { UserId = int.Parse(userId) });

            return View("UserPassData", passData);
        }

        public async Task<IActionResult> SelectNew()
        {
            ViewBag.Title = "New User PassData";

            return View("UserPassDataNew", new PassDataModel());
        }

        public async Task<IActionResult> Insert(PassDataModel newPassData)
        {
            ViewBag.Title = "Add new user PassData";

            if (await _mediator.Send(new ExistPassQuery(newPassData.Password)))
            {
                ViewBag.ErrorMessage = "This password exists in the database, to save the data enter another one.";

                return View("UserPassDataNew", newPassData);
            }

            var passData = await _mediator.Send(new AddUserPassDataCommand()
            { 
                Description = newPassData.Description,
                Login = newPassData.Login,
                Name = newPassData.Name,
                Password = newPassData.Password,
                UserId = int.Parse(_userManager.GetUserId(User))
            });

            return RedirectToAction("PassDataLoad", "UserPassData");
        }

        public async Task<IActionResult> SelectDelete(string Id)
        {
            ViewBag.Title = "Delete User PassData";

            var passData = await _mediator.Send(new PassDataQuery() { Id = int.Parse(Id)});

            return View("UserPassDataDelete", passData);
        }

        public async Task<IActionResult> Delete(string Id)
        {
            ViewBag.Title = "Delete User PassData";

            await _mediator.Publish(new DeletePassDataCommand() { Id = int.Parse(Id) });

            return RedirectToAction("PassDataLoad", "UserPassData");
        }

        public async Task<IActionResult> SelectEdit(string Id)
        {
            ViewBag.Title = "Edit User PassData";

            var passData = await _mediator.Send(new PassDataQuery() { Id = int.Parse(Id) });

            return View("UserPassDataEdit", passData);
        }

        public async Task<IActionResult> Edit(PassDataModel passDataModel)
        {
            ViewBag.Title = "Edit User PassData";

            if (await _mediator.Send(new ExistPassQuery(passDataModel.Password)))
            {
                ViewBag.ErrorMessage = "This password exists in the database, to save the data enter another one.";

                return View("UserPassDataEdit", passDataModel);
            }

            var passData = await _mediator.Send(new UpdateUserPassDataCommand() 
            {  
                Id = passDataModel.Id,
                Description = passDataModel.Description,
                Name = passDataModel.Name,
                Login = passDataModel.Login,
                Password = passDataModel.Password,
                UserId = int.Parse(_userManager.GetUserId(User))
            });
            
            return RedirectToAction("PassDataLoad", "UserPassData");
        }

        public IActionResult GetPasswordEdit(PassDataModel passData)
        {
            ViewBag.Title = "Edit User PassData";

            passData.Password = _passGenerator.GeneratePassword(true, true, true, true, 10);

            return View("UserPassDataEdit", passData);
        }

        public IActionResult GetPasswordNew(PassDataModel passData)
        {
            ViewBag.Title = "New User PassData";

            passData.Password = _passGenerator.GeneratePassword(true, true, true, true, 10);
        
            return View("UserPassDataNew", passData);
        }
    }
}
